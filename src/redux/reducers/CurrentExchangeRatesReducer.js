const currentExchangeRatesReducer = (state = null, action) => {
    switch (action.type) {

        case "CURRENT_EXCHANGE_RATES_SUCCESS":
            return action.payload;

        default:
            return state;
    }
};

export default currentExchangeRatesReducer;
