import axios from "axios";

const AuthActionType = {
    REGISTER_SUCCESS: "REGISTER_SUCCESS",
    REGISTER_FAIL: "REGISTER_FAIL",
    LOGOUT_SUCCESS: "LOGOUT_SUCCESS",
    LOGOUT_FAIL: "LOGOUT_FAIL",
    LOGIN_SUCCESS: "LOGIN_SUCCESS",
    LOGIN_FAIL: "LOGIN_FAIL",
};

const RegisterAuthAction = (userState, history, setErrorHandler) => {
    return async (dispatch) => {
        try {
            const res = await axios.post("signup", userState);
            const {data} = res;
            dispatch({type: AuthActionType.REGISTER_SUCCESS, payload: data});
            setErrorHandler({
                hasError: false,
                message: 'OK',
            });
        } catch (error) {
            if (error.response) {
                dispatch({
                    type: AuthActionType.REGISTER_FAIL,
                    payload: error.response.data.message,
                });
                setErrorHandler({
                    hasError: true,
                    message: error.response.data.message,
                });
            }
        }
    };
};

const LoginAuthAction = (loginState, history, setErrorHandler) => {
    return async (dispatch) => {
        try {
            const res = await axios.post("signin", loginState);
            const {data} = res;
            dispatch({type: AuthActionType.LOGIN_SUCCESS, payload: data});
            history.push("/currentExchangeRates");

        } catch (error) {
            if (error.response) {
                dispatch({
                    type: AuthActionType.LOGIN_FAIL,
                    payload: error.response.data.message,
                });
            }
            setErrorHandler({hasError: true, message: error.response.data.message});
        }
    };
};

const LogOutAuthAction = (history) => {
    // return async (dispatch) => {
    //   try {
    //     const res = await axios.get("signout");
    //     const { data } = res;
    //     dispatch({type: AuthActionType.LOGOUT_SUCCESS, payload: data.message,});
    //     history.push("/info");
    //   } catch (error) {
    //     if (error.response) {
    //       dispatch({
    //         type: AuthActionType.LOGOUT_FAIL,
    //         payload: error.response.data.message,
    //       });
    //     }
    //   }
    return async (dispatch) => {
        dispatch({type: AuthActionType.LOGOUT_SUCCESS, payload: 'Logout successful!',});
        history.push("/signIn");
    };
};

export {
    RegisterAuthAction,
    AuthActionType,
    LogOutAuthAction,
    LoginAuthAction,
};
